import Vue from 'vue';
import App from './components/App.vue';
import cachePlugin from './plugins/cache-plugin';
import store from './store';
import router from './router';

import './css/all.scss';

Vue.use(cachePlugin);

new Vue({
  el: '#app',
  store: store,
  router: router,
  render: (h) => h(App),
});
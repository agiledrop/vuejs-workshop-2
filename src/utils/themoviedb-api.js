import axios from 'axios';

export default class TheMovieDbApi {

  constructor() {
    this.apiKey = process.env.API_KEY;
    this.baseUrl = 'https://api.themoviedb.org/3';
  }

  getPopularItems(itemType) {
    return new Promise((resolve, reject) => {
      if (['tv', 'movie', 'people'].indexOf(itemType) === -1) {
        reject('Only "tv" or "movie" types are allowed.');
      }

      axios.get(`${this.baseUrl}/${itemType}/popular?api_key=${this.apiKey}`)
        .then((response) => resolve(response.data))
        .catch((reason) => reject(reason));
    });
  }

}

import Vue from 'vue';
import VueRouter from 'vue-router';
import HomePage from '../components/pages/HomePage.vue'
import MoviesPage from '../components/pages/MoviesPage.vue'

import store from '../store'
import MovieDetailPage from '../components/pages/MovieDetailPage.vue'
import RatingPage from '../components/pages/RatingPage.vue'
import ActorsPage from '../components/pages/ActorsPage.vue'

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      name: 'home',
      path: '/',
      component: HomePage,
    },
    {
      name: 'movie-list',
      path: '/movies',
      component: MoviesPage,
    },
    {
      path: '/dashboard',
      component: HomePage,
      beforeEnter: (to, from, next) => {
        if (store.getters.isUserLoggedIn) {
          next();
        } else {
          next({name: 'movie-list'})
        }
      }
    },
    {
      path: '/movie/:id',
      component: MovieDetailPage,
      children: [
        {
          path: 'ratings',
          component: RatingPage,
        },
        {
          path: 'actors',
          component: ActorsPage,
        },
      ],
    }
  ],
});

export default router;


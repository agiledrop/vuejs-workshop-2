import Cache from '../utils/cache'
import Icon from '../components/atoms/Icon.vue'

const cachePlugin = {
  install: (Vue, options) => {
    Vue.prototype.$cache = new Cache();
    Vue.component('icon', Icon);
    Vue.helloWorld = () => {
      console.log('hello world');
    }
  }
}

export default cachePlugin;

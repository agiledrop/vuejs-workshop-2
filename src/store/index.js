import Vue from 'vue';
import Vuex from 'vuex';
import formStore from './form.store'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: null,
  },
  getters: {
    isUserLoggedIn(state) {
      return state.user !== null;
    }
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    }
  },
  actions: {
    setUserAction(context, user) {
      context.commit('setUser', user);
    }
  },
  modules: {
    formStore: formStore,
  },
});

export default store;

const formStore = {
  namespaced: true,
  state: () => ({
    firstName: '',
    lastName: '',
    address: '',
  }),
  mutations: {
    setFirstName(state, value) {
      state.firstName = value;
    },
    setLastName(state, value) {
      state.lastName = value;
    },
    setAddress(state, value) {
      state.address = value;
    }
  },
  actions: {},
};

export default formStore;

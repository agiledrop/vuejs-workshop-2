export const sampleData = {
  discoverItems: Array(15).fill({
    date: "2019-11-12",
    rating: 85,
    title: "The Mandalorian",
    link: "https://www.themoviedb.org/tv/82856",
    imageUrl: "https://image.tmdb.org/t/p/w220_and_h330_face/BbNvKCuEF4SRzFXR16aK6ISFtR.jpg",
  }),
};

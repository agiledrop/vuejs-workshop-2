export const languageList = [
  {
    value: 'en-US',
    text: 'English (en-US)',
  },
  {
    value: 'de-DE',
    text: 'German (de-DE)',
  },
];
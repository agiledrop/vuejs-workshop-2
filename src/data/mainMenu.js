export const mainMenu = [
  {
    label: 'Movies',
    children: [
      {
        label: 'Popular',
        url: '/movies',
      },
      {
        label: 'Now Playing',
        url: 'http://example.com',
      },
      {
        label: 'Upcoming',
        url: 'http://example.com',
      },
      {
        label: 'Top Rated',
        url: 'http://example.com',
      },
    ],
  },
  {
    label: 'TV Shows',
    children: [
      {
        label: 'Popular',
        url: 'http://example.com',
      },
      {
        label: 'Airing Today',
        url: 'http://example.com',
      },
      {
        label: 'On TV',
        url: 'http://example.com',
      },
      {
        label: 'Top Rated',
        url: 'http://example.com',
      },
    ],
  },
  {
    label: 'People',
    children: [
      {
        label: 'Popular People',
        url: 'http://example.com',
      },
    ],
  },
  {
    label: 'More',
    children: [
      {
        label: 'Discussions',
        url: 'http://example.com',
      },
      {
        label: 'Leaderboard',
        url: 'http://example.com',
      },
      {
        label: 'Support',
        url: 'http://example.com',
      },
      {
        label: 'API',
        url: 'http://example.com',
      },
    ],
  },
];
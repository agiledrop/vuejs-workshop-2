### VueJS Onboarding project

This project is a rebuild of http://themoviedb.org website in VueJS. It is a work in progress.


#### Installation & Setup

NodeJS v12 and Yarn package manager are required for this project.

- Obtain a v3 API key from http://themoviedb.org
- Copy `.env.example` to `.env` then edit `.env` and fill in the API key 
  variable  
- Run `npm install` from project root to install libraries
- To run a local development server run `npm run dev`
- To compile the app run `npm run prod` and it will build the files in `dist/`
  folder